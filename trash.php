<?php

    require_once('css.php');
    include "db.php";

    $query = "SELECT * FROM trashes";
    $result = mysqli_query($connection, $query);


    if(isset($_GET['delete'])){
        $delete =   $_GET['delete'];         
        $deletequery = "DELETE FROM trashes WHERE trash_id = $delete";
        $deletequery = mysqli_query($connection, $deletequery);

        if(!$deletequery){
            die("failed");
        }else{
            header('location: trash.php?todo-deleted');
        }


    }

    if(isset($_GET['restore'])){
        $restore =   $_GET['restore']; 
        $updateQ =  "insert into todo (t_name) select trash_todo from trashes where trash_id='" . $restore . "'";  
        $updateQ = mysqli_query($connection, $updateQ);
        $deletequery = "DELETE FROM trashes WHERE trash_id = $restore";
        $deletequery = mysqli_query($connection, $deletequery);

        if(!$deletequery){
            die("failed");
        }else{
            header('location: trash.php?todo-restored');
        }


    }



?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
 <title>Document</title>
</head>
<body>
    <d v class="row">
        <div class="col-lg-3">
            <div class="sidebar">
                <header>My Todo lists</header>
                    <ul>
                        <li ><a href="update.php"><i class="fas fa-sticky-note"></i>Update</a></li>
                        <li><a href="cover.php"><i class="fas fa-tasks"></i>Tasks</a></li>
                         <li><a class="active" href="trash.php"><i class="far fa-trash-alt"></i>Trash</a></li>
                

                    </ul>
            </div>
        </div>

        
       <div class="col-lg-9">
           <div class="container">
               
               <h1 style="width:60%; margin-left: 20%" class="text-center"><marquee>TIAGO's  TODO TRASHES</marquee></h1>
               
           </div>
        <div class="table-responsive">
            <table class="table table-border table-striped table-hover">
                <thead>
                    <th>ID</th>
                    <th>TODO LISTS</th>
                    <th>DATE ADDED</th>
                    <th>RESTORE</th>
                    <th>DELETE TODO</th>       
                </thead>
                <tbody>
                    <?php
                        while($row = mysqli_fetch_assoc($result)){
                            $id = $row['trash_id'];
                            $name = $row['trash_todo'];
                            $date = $row['trash_date'];
                            ?>
                    <tr>
                        <td><?php echo $id; ?></td>
                        <td><?php echo $name; ?></td>
                        <td><?php echo $date; ?></td>
                        <td><a href="trash.php?restore=<?php echo $id; ?>" class="btn btn-success" name="restore"><i class="fas fa-trash-restore"></i>restore</a></td>
                        <td><a href="trash.php?delete=<?php echo $id; ?>" class="btn btn-danger" name="delete"><i class="far fa-trash-alt"></i> delete</a></td>
                    </tr>
                      <?php  }
                    
                    ?>

                   
                </tbody>
            </table>
        </div>
    </div>
</div>
   

       

    </div>

    <script>
        $('.check1').change(function(){
    

            if($(this).prop("checked") == true){
                $(this).parent().parent().css("text-decoration","line-through");
           }
            else if($(this).prop("checked") == false){
            $(this).parent().parent().css("text-decoration","none");
        }
        });
    </script>

</body>
</html>