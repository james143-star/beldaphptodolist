<?php

    require_once('css.php');
    include "db.php";

    $query = "SELECT * FROM todo";
    $result = mysqli_query($connection, $query);


    if(isset($_POST['submit'])){
        $todo = $_POST['todo'];
        $sql = "INSERT INTO todo (t_name) values ('$todo');";
        $resulta = mysqli_query($connection, $sql);

        if(!$resulta){
            die("failed");
        }else{
            header('location: cover.php?todo-added');
        }
    }

    if(isset($_GET['delete'])){
        $delete =   $_GET['delete']; 
        $insertquery =  "insert into trashes (trash_todo) select t_name from todo where t_id='" . $_GET["delete"] . "'";  
        $insertquery = mysqli_query($connection, $insertquery);
        $deletequery = "DELETE FROM todo WHERE t_id = $delete";
        $deletequery = mysqli_query($connection, $deletequery);

        if(!$deletequery){
            die("failed");
        }else{
            header('location: cover.php?todo-deleted');
        }


    }



?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
 <title>Document</title>
</head>
<body>
    <div class="row">
        <div class="col-lg-3">
            <div class="sidebar">
                <header>My Todo lists</header>
                    <ul>
                        <li ><a href="update.php"><i class="fas fa-sticky-note"></i>Update</a></li>
                        <li><a class="active" href="cover.php"><i class="fas fa-tasks"></i>Tasks</a></li>
                         <li><a href="trash.php"><i class="far fa-trash-alt"></i>Trash</a></li>
                

                    </ul>
            </div>
        </div>
       
        <div class="col-lg-9">
        <div class="container">  
        <div class="todo">
            <h1>TIAGO TODO LISTS</h1>
            <h3> Add New Todo</h3>
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                <div class="form-group">
                    <input style="width: 300px;" type="text" class="form-control" name="todo"  placeholder="todo name...">
                </div>
                <div class="form-group">
                    <input style="width: 200px; margin-left: 20%;" type="submit" name="submit" class="btn btn-primary" value="Add todo list">
                </div>

            </form>
        </div>
        <div class="table-responsive">
            <table class="table table-border table-striped table-hover">
                <thead>
                    <th>ID</th>
                    <th>TODO LISTS</th>
                    <th>DATE ADDED</th>
                    <th>UPDATE</th>
                    <th>DELETE TODO</th>       
                </thead>
                <tbody>
                    <?php
                        while($row = mysqli_fetch_assoc($result)){
                            $id = $row['t_id'];
                            $name = $row['t_name'];
                            $date = $row['t_date'];
                            ?>
                    <tr>
                        <td><?php echo $id; ?></td>
                        <td>  <input class="check1" type="checkbox" name="checkbox" value="Bike"> <?php echo $name; ?></td>
                        <td><?php echo $date; ?></td>
                        <td><a href="update.php?update=<?php echo $id; ?>" class="btn btn-primary" name="edit"><i class="far fa-edit"></i>update</a></td>
                        <td><a href="cover.php?delete=<?php echo $id; ?>" class="btn btn-danger" name="delete"><i class="far fa-trash-alt"></i> delete</a></td>
                    </tr>
                      <?php  }
                    
                    ?>

                   
                </tbody>
            </table>
        </div>
    </div>
        </div>  
       

    </div>

    <script>
        $('.check1').change(function(){
    

            if($(this).prop("checked") == true){
                $(this).parent().parent().css("text-decoration","line-through");
           }
            else if($(this).prop("checked") == false){
            $(this).parent().parent().css("text-decoration","none");
        }
        });
    </script>

</body>
</html>