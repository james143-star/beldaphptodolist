<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <style>
        .todo{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            border-radius: 3px;
            border: 1px solid #000;
            margin-top: 5px;
        }
        body{
            font-family: sans-serif;
        }
        *{
            margin: 0;
            padding: 0;
            list-style: none;
            text-decoration: none;
        }
        .sidebar{
            position: fixed;
            left: 0;
            width: 250px;
            height: 100%;
            background: darkcyan;
        }
        .sidebar header{
            font-size: 22px;
            color: white;
            text-align: center;
            line-height: 70px;
            background: #063146;
            user-select: none;
            
        }
        .sidebar ul a{
            display: block;
            height: 100%;
            width: 100%;
            line-height: 65px;
            font-size: 20px;
            color: white;
            padding-left: 40px;
            box-sizing: border-box;
            border-top: 1px solid rgba(255,255,255,.1);
            border-bottom: 1    px solid black;
            transition: .4s;
        }
        ul li:hover a{
            padding-left: 50px;
           
            color: wheat;
        }
        .sidebar ul a i{
            margin-right: 16px;
        }
        .section{
            background-image: url(bg.jpeg) no repeat;
            background-position: center;
            background-size: cover;
            height: 100vh;

        }
        .active{    
            font-size: 20px;
            background-color: orange;
        }

        #todolist:checked + label {
            color:blue;
            text-decoration: line-through;
        }
        .text-center{
            color: yellow;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            border-radius: 3px;
            background-color: gray;
            margin-top: 5px;
        }
        
        
    </style>
</body>
</html>